package ordination;

import java.time.*;

public class DagligFast extends Ordination {

	private Dosis[] doser = new Dosis[4];
	private double[] maksPrDøgn = new double[4];
	private final LocalTime[] tider = new LocalTime[] { LocalTime.of(8, 0), LocalTime.of(12, 0), LocalTime.of(18, 0),
			LocalTime.of(22, 0) };

	public DagligFast(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
	}

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel,
			double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		super(startDen, slutDen);
		maksPrDøgn = new double[] { morgenAntal, middagAntal, aftenAntal, natAntal };
		double sum = 0;
		for (double doseMængde : maksPrDøgn) {

			sum += doseMængde;

			if (doseMængde < 0) {
				throw new IllegalArgumentException("negativ mængde af dose");
			} else if (sum > 4) {
				throw new IllegalArgumentException("sum af doser er over 4");
			}
		}
		for (int j = 0; j < tider.length; j++) {
			createDosis(new Dosis(tider[j], maksPrDøgn[j]));
		}
		setLaegemiddel(laegemiddel);
	}

	@Override
	public double samletDosis() {
		return 0;
	}

	@Override
	public double doegnDosis() {
		return 0;
	}

	@Override
	public String getType() {
		return null;
	}

	public void createDosis(Dosis dosis) {

		for (Dosis dosisIArray : doser) {
			
			if (dosis==null) {
				continue;
			}
	
			if (dosisIArray.equals(dosis)) {
				return;
			}

		}

		for (int i = 0; i < doser.length; i++) {
			if (doser[i].equals(null)) {
				doser[i] = dosis;
				return;
			}

		}

		System.err.println("Dosis array er fylt op");

	}

	public Dosis[] getDoser() {
		return doser;
	}

	public void removeDosis(Dosis dosis) {

		for (int i = 0; i < doser.length; i++) {
			if (doser[i].equals(dosis)) {
				doser[i] = null;
				return;
			}

		}

	}

}
