package main;

import java.time.LocalDate;

import controller.Controller;
import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;

public class Main {

	public static Patient dummyPatient = new Patient("231045-0637", "Kim", 70);
	public static Laegemiddel dummyLaegemiddel = new Laegemiddel("Morfin", 0.01, 0.01, 0.02, "mg");
	public static LocalDate start = LocalDate.of(2000, 1, 1);
	public static LocalDate slut = LocalDate.of(2000, 1, 14);

	public static void main(String[] args) {

		//testPn();
		testDf();
	}

	public static void testPn() {

		PN pn = Controller.opretPNOrdination(start, slut, dummyPatient, dummyLaegemiddel, 10);

		System.out.println(pn.antalDage());
		pn.addTidspunkt(LocalDate.of(2000, 1, 5));
		System.out.println(pn.doegnDosis());

		System.out.println(pn.getAntalEnheder() + " " + pn.getType() + " " + pn.getAntalGangeGivet() + " "
				+ pn.getLaegemiddel() + " " + pn.samletDosis());

	}
	
	public static void testDf() {
		DagligFast df  = Controller.opretDagligFastOrdination(start, slut, dummyPatient, dummyLaegemiddel,
				1, 1, 0, 2);
	}
}
