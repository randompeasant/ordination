package main;
//hej
import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class Tester {

	public static Patient dummyPatient = new Patient("231045-0637", "Kim", 70);
	public static Laegemiddel dummyLaegemiddel = new Laegemiddel("Morfin", 0.01, 0.01, 0.02, "mg");
	public static LocalDate start = LocalDate.of(2000, 1, 1);
	public static LocalDate slut = LocalDate.of(2000, 1, 14);

	@Test
	public void testPn() {
		PN pn;

		try {
			pn = Controller.opretPNOrdination(start, slut, dummyPatient, dummyLaegemiddel, 10);
		} catch (Exception e) {
			fail();
			return;
		}

		assertEquals(14, pn.antalDage());

		try {
			pn.addTidspunkt(LocalDate.of(2000, 1, 5));
		} catch (Exception e) {
			fail();
			return;
		}

		assertEquals((double) 10 / 14, pn.doegnDosis(), 0.2);

		try {
			System.out.println(pn.getAntalEnheder() + " " + pn.getType() + " " + pn.getAntalGangeGivet() + " "
					+ pn.getLaegemiddel());
		} catch (Exception e) {
			fail();
			return;
		}

	}

	@Test
	public void testDf() {

		DagligFast df;
		try {
			System.out.println("start");
			df = Controller.opretDagligFastOrdination(start, slut, dummyPatient, dummyLaegemiddel, 1, 1, 0, 2);

			df.getSlutDen();
		} catch (Exception e) {
			fail();
			return;
		}

	}

	@Test
	public void testDs() {
		
		DagligSkaev ds;  
		try {
			
			ds  = Controller.opretDagligSkaevOrdination(start, slut, 
					dummyPatient, dummyLaegemiddel,
					new LocalTime[] { LocalTime.of(10, 30), LocalTime.of(18, 30) },
					new double[] { 2.0, 2.5 });
			
			System.out.println(ds.getLaegemiddel() + " " + ds.getType());
		} catch (Exception e) {
			fail();
			return;
		}

	
	}

}
